package pl.course.tomek;

public class DeluxeBurger extends Hamburger {
    private double chips;
    private double drinks;

    public DeluxeBurger(String name, String rollType, String meat, double price) {
        super(name, rollType, meat, price);
        this.chips = 1.5;
        this.drinks = 2;
    }

    @Override
    public void setLettuce(boolean lettuce) {
        super.setLettuce(false);
        System.out.println("Cannot add additional items to deluxe burger");
    }

    @Override
    public void setTomato(boolean tomato) {
        super.setTomato(false);
        System.out.println("Cannot add additional items to deluxe burger");
    }

    @Override
    public void setCarrot(boolean carrot) {
        super.setCarrot(false);
        System.out.println("Cannot add additional items to deluxe burger");
    }

    @Override
    public void setBeckon(boolean beckon) {
        super.setBeckon(false);
        System.out.println("Cannot add additional items to deluxe burger");
    }

    @Override
    public double info() {

        double total = super.info();

        System.out.println("chips " + chips + "$");
        total += chips;

        System.out.println("drinks " + drinks + "$");
        total += drinks;

        System.out.println("total " + String.format("%.2f", total) + "$");
        return total;
    }
}
