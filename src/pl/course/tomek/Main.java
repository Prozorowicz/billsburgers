package pl.course.tomek;

public class Main {

    public static void main(String[] args) {
        Hamburger hamburger = new Hamburger("hamburger", "white", "brisket", 9.99);
        hamburger.setBeckon(true);
        hamburger.info();
        HealthyBurger healthyBurger = new HealthyBurger("healthy burger", "chuck", 12.99);
        healthyBurger.setCarrot(true);
        healthyBurger.setKale(true);
        healthyBurger.info();
        DeluxeBurger deluxeBurger = new DeluxeBurger("deluxe burger", "multi-grain", "dry-aged beef", 15.99);
        deluxeBurger.info();

    }
}
