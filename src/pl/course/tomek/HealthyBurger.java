package pl.course.tomek;

public class HealthyBurger extends Hamburger {
    private boolean kale;
    private double kalePrice = 1.50;
    private boolean spinach;
    private double spinachPrice = 2.50;

    public HealthyBurger(String name, String meat, double price) {
        super(name, "brown rye", meat, price);
    }

    public void setKale(boolean kale) {
        this.kale = kale;
    }

    public void setSpinach(boolean spinach) {
        this.spinach = spinach;
    }

    @Override
    public double info() {
        double total = super.info();
        if (kale) {
            System.out.println("kale " + kalePrice + "$");
            total += kalePrice;
        }
        if (spinach) {
            System.out.println("spinach " + spinachPrice + "$");
            total += spinachPrice;
        }
        System.out.println("total " + total + "$");
        return total;
    }
}
